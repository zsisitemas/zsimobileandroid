package com.example.zsimobile.service;

import com.example.zsimobile.model.ProdutoDTO;
import com.example.zsimobile.model.ProdutoModel;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ProdutoService {

    @GET("api/v1/produtos")
    Observable<ProdutoDTO> buscarProdutos();

}
