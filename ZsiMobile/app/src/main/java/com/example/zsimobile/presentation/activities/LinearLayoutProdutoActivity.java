package com.example.zsimobile.presentation.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zsimobile.R;
import com.example.zsimobile.model.ProdutoDTO;
import com.example.zsimobile.model.ProdutoModel;
import com.example.zsimobile.presentation.adapter.CartListAdapter;
import com.example.zsimobile.presentation.adapter.viewholder.MyViewHolder;
import com.example.zsimobile.retrofit.RetrofitConfig;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LinearLayoutProdutoActivity extends AppCompatActivity implements RecyclerProdutoTouchHelper.RecyclerItemTouchHelperListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private List<ProdutoModel> listaProdutos;
    private CartListAdapter mAdapter;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_produto);

        getSupportActionBar().setTitle(getString(R.string.my_cart));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recycler_view_layour_recycler);
        coordinatorLayout = findViewById(R.id.coordinator_layout);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // adcionando produto touch helper
        // apenas ItemTouchHelper.LEFT adcionado para detectar Direita para Esquerda deslizar
        // se voce quiser ambos Direita -> Esquerda and Esquerda -> Direita
        // adcione pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT como parametro
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerProdutoTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        // fazendo chamada webservice
        prepareCart();
    }

    /**
     * metodo para fazer chamada rxjava e analisar json
     */
    private void prepareCart() {

        Observable<ProdutoDTO> observable = new RetrofitConfig().getProdutoService().buscarProdutos();
        observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProdutoDTO>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("ProdutoService   ", "Erro ao buscar o produtos:" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProdutoDTO produtoDTO) {
                        ProdutoDTO data = produtoDTO;
                        listaProdutos = data.getData();

                        mAdapter = new CartListAdapter(LinearLayoutProdutoActivity.this, listaProdutos);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        mRecyclerView.setLayoutManager(mLayoutManager);
                        mRecyclerView.setAdapter(mAdapter);

                    }
                });
    }

    /**
     * retorno quando recycler view e deslizado
     * produto sera removido ao deslizar
     * opcao Undo sera providenciada no snackbar para restaurar o produto
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof MyViewHolder) {
            // pegar o nome do produto removido para mostra-lo no snack bar
            String name = listaProdutos.get(viewHolder.getAdapterPosition()).getNome();

            // backup do produto removido para Undo proposito
            final ProdutoModel deletedProduto = listaProdutos.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remover o produto da recycler view
            mAdapter.removeProduto(viewHolder.getAdapterPosition());

            // mostrando snack bar com opcao Undo
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removido do Carrinho!", Snackbar.LENGTH_LONG);
            snackbar.setAction("DESFAZER", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo esta selecionado, restaure o produto selecionado
                    mAdapter.restoreProduto(deletedProduto, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflar o menu; isto adciona listaProdutos para o action bar se estiver presente.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}

