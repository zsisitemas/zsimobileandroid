package com.example.zsimobile.presentation.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.zsimobile.R;
import com.example.zsimobile.model.ProdutoModel;
import com.example.zsimobile.presentation.adapter.viewholder.MyViewHolder;

import java.util.List;

public class CartListAdapter extends RecyclerView.Adapter<MyViewHolder> {
    private Context context;
    private final List<ProdutoModel> mProdutos;

    public CartListAdapter(Context context, List<ProdutoModel> mProdutos) {
        this.context = context;
        this.mProdutos = mProdutos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View produtoView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_list_produto, parent, false);

        return new MyViewHolder(produtoView);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ProdutoModel produto = mProdutos.get(position);
        holder.name.setText(produto.getNome());
        holder.description.setText(produto.getDescricao());
        holder.price.setText("R$" + produto.getPrecoVenda());

        Glide.with(context)
                .load(produto.getImagem())
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return mProdutos != null ? mProdutos.size() : 0;
    }

    public void removeProduto(int position) {
        mProdutos.remove(position);
        // notificar produto removido pela posicao
        // para executar animacoes deletar recycler view
        // NOTA: nao chame notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreProduto(ProdutoModel produto, int position) {
        mProdutos.add(position, produto);
        // notificar produto adcionado pela posicao
        notifyItemInserted(position);
    }
}
