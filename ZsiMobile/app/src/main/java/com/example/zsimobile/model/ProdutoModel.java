package com.example.zsimobile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ProdutoModel {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("descricao")
    @Expose
    private String descricao;
    @SerializedName("valor")
    @Expose
    private Object valor;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("categoria")
    @Expose
    private Object categoria;
    @SerializedName("representante")
    @Expose
    private Object representante;
    @SerializedName("fone_representante")
    @Expose
    private Object foneRepresentante;
    @SerializedName("estoque")
    @Expose
    private Object estoque;
    @SerializedName("estoque_Mini")
    @Expose
    private Object estoqueMini;
    @SerializedName("estoque_Max")
    @Expose
    private Object estoqueMax;
    @SerializedName("preco_compra")
    @Expose
    private Object precoCompra;
    @SerializedName("preco_custo")
    @Expose
    private Object precoCusto;
    @SerializedName("preco_venda")
    @Expose
    private float precoVenda;
    @SerializedName("validade")
    @Expose
    private Object validade;
    @SerializedName("sub_categoria")
    @Expose
    private Object subCategoria;
    @SerializedName("unidade")
    @Expose
    private Object unidade;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("imagem")
    @Expose
    private String imagem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getCategoria() {
        return categoria;
    }

    public void setCategoria(Object categoria) {
        this.categoria = categoria;
    }

    public Object getRepresentante() {
        return representante;
    }

    public void setRepresentante(Object representante) {
        this.representante = representante;
    }

    public Object getFoneRepresentante() {
        return foneRepresentante;
    }

    public void setFoneRepresentante(Object foneRepresentante) {
        this.foneRepresentante = foneRepresentante;
    }

    public Object getEstoque() {
        return estoque;
    }

    public void setEstoque(Object estoque) {
        this.estoque = estoque;
    }

    public Object getEstoqueMini() {
        return estoqueMini;
    }

    public void setEstoqueMini(Object estoqueMini) {
        this.estoqueMini = estoqueMini;
    }

    public Object getEstoqueMax() {
        return estoqueMax;
    }

    public void setEstoqueMax(Object estoqueMax) {
        this.estoqueMax = estoqueMax;
    }

    public Object getPrecoCompra() {
        return precoCompra;
    }

    public void setPrecoCompra(Object precoCompra) {
        this.precoCompra = precoCompra;
    }

    public Object getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(Object precoCusto) {
        this.precoCusto = precoCusto;
    }

    public float getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(float precoVenda) {
        this.precoVenda = precoVenda;
    }

    public Object getValidade() {
        return validade;
    }

    public void setValidade(Object validade) {
        this.validade = validade;
    }

    public Object getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(Object subCategoria) {
        this.subCategoria = subCategoria;
    }

    public Object getUnidade() {
        return unidade;
    }

    public void setUnidade(Object unidade) {
        this.unidade = unidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

}