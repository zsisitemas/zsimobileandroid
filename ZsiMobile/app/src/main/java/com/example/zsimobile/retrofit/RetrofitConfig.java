package com.example.zsimobile.retrofit;

import com.example.zsimobile.service.ProdutoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class RetrofitConfig {

    private final Retrofit retrofit;

    //configuraçao retrofit
    public RetrofitConfig(){
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.0.4:3000/")  //Base url "http://Endereço IPv4:3000/"
                .addConverterFactory(GsonConverterFactory.create())  //Converter json em string
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();   //Criar objeto retrofit
    }

    public ProdutoService getProdutoService(){
        return this.retrofit.create(ProdutoService.class);
    }

}
